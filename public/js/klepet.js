var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.posljiOsebnoSporocilo = function(kanal,vzdevek, tekst){
  var sporocilo = {
    kanal: kanal,
    vzdevek: vzdevek,
    besedilo: tekst
  };
  this.socket.emit('sporociloOsebno', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.spremeniKanalGeslo = function(kanal,geslo) {
  this.socket.emit('pridruzitevZahteva', {
  novKanal: kanal,
  geslo:geslo
  });
};

Klepet.prototype.divElementEnostavniTekst = function (sporocilo) {
  return $('<div style="font-weight: bold"></div>').append(sporocilo);
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      if (besede.length > 2 ){
  	    var kanal = besede[1].substring(1, besede[1].length-1);
   	    var narekovaj1 = besede[1].substring(0,1);
   	    var narekovaj2 = besede[1].substring(besede[1].length-1,besede[1].length);
     	  var geslo = besede[2].substring(1, besede[2].length-1);
     	  var narekovaj3 = besede[2].substring(0,1);
     	  var narekovaj4 = besede[2].substring(besede[2].length-1,besede[2].length);
     	  if (narekovaj1 == '"' && narekovaj2 == '"' && narekovaj3 == '"' && narekovaj4 == '"')
  		    this.spremeniKanalGeslo(kanal, geslo);
      }
      else{
		    var kanal = besede[1];
		    var geslo = ' ';
		  this.spremeniKanalGeslo(kanal,geslo);
	    }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
  	  if (besede.length > 2 ) {
   	    besede.shift();
   	    var vzdevek = besede.shift();
   	    var narekovaj1 = vzdevek.charAt(0);
   	    var narekovaj2 = vzdevek.charAt(vzdevek.length-1);
     	  vzdevek = vzdevek.substring(1, vzdevek.length-1);
     	  var tekst = besede.join(' ');
     	  var narekovaj3 = tekst.charAt(0);
     	  var narekovaj4 = tekst.charAt(tekst.length-1);
       	tekst = tekst.substring(1, tekst.length-1);
       	if (narekovaj1 == '"' && narekovaj2 == '"' && narekovaj3 == '"' && narekovaj4 == '"'){
          this.posljiOsebnoSporocilo($('#kanal').text(),vzdevek,tekst );
          $('#sporocila').append(divElementEnostavniTekst('(zasebno za ' + vzdevek + '): ' + tekst));
    	    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
       	}
    	  else {
    	    tekst = 'Neznan ukaz.';
		      $('#sporocila').append(divElementEnostavniTekst(tekst));
	        $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    	  }
	    }
	    else {
        tekst = 'Neznan ukaz.';
		    $('#sporocila').append(divElementEnostavniTekst(tekst));
	      $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
	    }
  	  break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};
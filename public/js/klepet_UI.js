function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').append(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

var Words = function(sporocilo){
  var Filter;
  var BadWords;
  for(var i = 0; i < SWords.length; i++){
    Filter = '';
    for(var k = 0; k < SWords[i].length; k++){
      Filter = Filter.concat('*');
    }
    BadWords = "\\b" + SWords[i] + "\\b";
    sporocilo = sporocilo.replace(new RegExp( BadWords, "gi"), Filter);
  }
  return sporocilo;
};

var Emoticons = ['<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png" alt="smile" />',
                 '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png" alt="sad" />"',
                 '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png" alt="wink" />"',
                 '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png" alt="like" />"',
                 '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png" alt="kiss" />',];
                 
var EmoLabels = [/\:\)/g, /\:\(/g, /\;\)/g, /\(y\)/g, /\:\*/g];

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    var besede = sporocilo.split(' ');
    var ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
    if (ukaz == 'zasebno')  {
       sporocilo = Words(sporocilo);
      sporocilo = sporocilo.replace('<','&lt');
      sporocilo = sporocilo.replace('>','&gt');
      for(var i = 0; i < Emoticons.length; i++){
        sporocilo = sporocilo.replace(EmoLabels[i], Emoticons[i]);
      }
    }
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    sporocilo = Words(sporocilo);
    var cleanTekst =  sporocilo.replace(/<(?:.|\n)*?>/gm,"");
    var tekst = cleanTekst;

    tekst = tekst.replace(":)", "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png' alt='smile' />");
    tekst = tekst.replace(":(", "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png' alt='sad' />");
    tekst = tekst.replace(";)", "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png' alt='wink' />");
    tekst = tekst.replace(":*", "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png' alt='kiss' />");
    tekst = tekst.replace("(y)", "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png' alt='like' />");

    klepetApp.posljiSporocilo($('#kanal').text(), tekst);
    $('#sporocila').append(divElementEnostavniTekst(tekst));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();
var SWords;

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  SWords = $('<div>').load("../swearWords.txt", function() {
    SWords = SWords.text().split("\n");
  });

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').append(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });
  
  socket.on('sporociloOsebno', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('uporabniki', function(uporabniki) {
	  var size = uporabniki.size;
    var index = uporabniki.index;
    var uporabnik =uporabniki.uporabnik;
	  if (uporabniki.size == -1 )
      $('#seznam-uporabnikov').empty();
    if (uporabniki.size > 0 )
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki.uporabnik));
    for(var user in uporabniki.uporabniki){
      if (user != ''){
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(user));
      }
    }
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);
  
  setInterval(function() {
    socket.emit('uporabniki');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
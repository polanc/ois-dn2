var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];

var trenutniKanal = {};
var imenaGledeNaSocket = [];

var geslaKanalov=[];
var imenaKanalov =[];
var imenaKanalovDim = 10;
var indexSteviloKanalov = 0;
var imenaKanalovGeslo = new Array(10);

for (var i = 0; i < imenaKanalovDim; i++) {
    imenaKanalovGeslo[i] = new Array(2);
}

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  imenaKanalovGeslo[0][0] =  'Skedenj';
  imenaKanalovGeslo[0][1] =  ' ';
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajPosredovanjeOsebnegaSporocila(socket, vzdevkiGledeNaSocket);
    obdelajPridruzitevKanalu(socket,geslaKanalov,imenaKanalov,imenaKanalovGeslo,imenaKanalovDim,indexSteviloKanalov);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    socket.on('uporabniki', function() {
      obdelajPosredovanjeUporabnikov(socket,vzdevkiGledeNaSocket,trenutniKanal[socket.id]);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });


  
};
 
function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
 var vzdevek = 'Gost' + stGosta;
 vzdevki[socket.id] = vzdevek;
 socket.emit('vzdevekSpremembaOdgovor', {
   uspesno: true,
   vzdevek: vzdevek
 });
 uporabljeniVzdevki.push(vzdevek);
 return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: vzdevkiGledeNaSocket[socket.id] + ' @ ' + kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
        socket.emit('uporabniki', {besedilo: vzdevkiGledeNaSocket[uporabnikSocketId]}); //ni blo pri 2_7
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        trenutniKanal[socket.id];
        socket.emit('pridruzitevOdgovor',{ kanal: vzdevkiGledeNaSocket[socket.id] + ' @ ' + trenutniKanal[socket.id]});
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    var kanal1 = sporocilo.kanal.substring(sporocilo.kanal.indexOf(' @ ') + 3, sporocilo.kanal.length);
    if (sporocilo.besedilo.length > 0)
      socket.broadcast.to(kanal1).emit('sporocilo', {besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo});
  });
}

function obdelajPosredovanjeOsebnegaSporocila(socket) {
  socket.on('sporociloOsebno', function (sporocilo){
  	var besedilo= ' ';
  	if (uporabljeniVzdevki.indexOf(sporocilo.vzdevek) != -1 && vzdevkiGledeNaSocket[socket.id] != sporocilo.vzdevek && sporocilo.besedilo.length != 0 ){
      besedilo =  sporocilo.besedilo;
  		var uporabniki = io.sockets.clients();
  		if (uporabniki.length > 1){
  			for (var i in uporabniki){
  		    var uporabnikiSocketId = uporabniki[i].id;
  			  if (vzdevkiGledeNaSocket[uporabnikiSocketId] == sporocilo.vzdevek){
  			    io.sockets.socket(uporabnikiSocketId).emit('sporociloOsebno', { besedilo: vzdevkiGledeNaSocket[socket.id] + ' (zasebno): ' + sporocilo.besedilo });
          }
  			}
  		}
  	}
  	else {
  		besedilo  = 'Sporočila "'+ sporocilo.besedilo + '" uporabniku z vzdevkom "' + sporocilo.vzdevek + '" ni bilo mogoče posredovati.';
  		socket.emit('sporociloOsebno',{ besedilo:besedilo } );
  	}
	});
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    socket.leave(trenutniKanal[socket.id]);
    pridruzitevKanalu(socket, kanal.novKanal);
  });
}

function obdelajPosredovanjeUporabnikov(socket,vzdevkiGledeNaSocket,kanal) {
    socket.emit('uporabniki', {size:-1, index: -1, uporabnik: ''});
	  var socketIds = io.sockets.manager.rooms['/'+kanal];
	  var uporabniki = [];
	  if (socketIds){
	    if (socketIds.length > 0 ){
		  for ( var i =0,len=socketIds.length;i < len;i++){
				if (vzdevkiGledeNaSocket[socketIds[i]]){
				 uporabniki.push( vzdevkiGledeNaSocket[socketIds[i]]);
				 socket.emit('uporabniki', {size:socketIds.length, index: i, uporabnik: vzdevkiGledeNaSocket[socketIds[i]]});
				}
		  }
	  }
  }
}

function obdelajPridruzitevKanalu(socket,gesla,imena,imenaKanalovGeslo,imenaKanalovDim,indexSteviloKanalov) {
   socket.on('pridruzitevZahteva', function(kanal) {
	var FindKanal = 0;
	var indexKanala = -1;
	for (var i = 0; i < indexSteviloKanalov + 1; i++){
		if  (imenaKanalovGeslo[i][0] ==  kanal.novKanal){
			indexKanala = i;
			if (imenaKanalovGeslo[i][1] ==  kanal.geslo ){
				FindKanal = 1;
				break;
			}
		}
	}
	if ( FindKanal == 1 ){
		socket.leave(trenutniKanal[socket.id]);
     pridruzitevKanalu(socket, kanal.novKanal);
	}
	else{
		if  (indexKanala >= 0){
		  var besedilo;
		  if ((imenaKanalovGeslo[i][1] == ' ') && (kanal.geslo !== '')){
		   besedilo = 'Izbrani kanal "' + kanal.novKanal + '" je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev <kanal> ali zahtevajte kreiranje kanala z drugim imenom.';
		  } else {
		    besedilo  = 'Pridružitev v kanal "' + kanal.novKanal + '" ni bilo uspešno, ker je geslo napačno!';
		  }
	    socket.emit('sporocilo', {besedilo:besedilo});
	  }
	  else{
		  socket.leave(trenutniKanal[socket.id]);
  	  pridruzitevKanalu(socket, kanal.novKanal);
			indexSteviloKanalov +=1;
			if (imenaKanalovDim <	indexSteviloKanalov){
				for (var i = 0; i < imenaKanalovDim; i++){
          imenaKanalovGeslo[imenaKanalovDim] = new Array(2);
          imenaKanalovDim +=1;
	      }
			}
			imenaKanalovGeslo[indexSteviloKanalov][0]=kanal.novKanal;
			imenaKanalovGeslo[indexSteviloKanalov][1]=kanal.geslo;
		}
	}
   });
 }

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}
